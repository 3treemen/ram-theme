<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Blossom_Shop
 */
    
    /**
     * After Content
     * 
     * @hooked blossom_shop_content_end - 20
    */
    do_action( 'blossom_shop_before_footer' );
    
    /**
     * Before footer
     * 
     * @hooked blossom_shop_instagram - 10
     * @hooked blossom_shop_newsletter - 20
    */
    do_action( 'blossom_shop_before_footer_start' );

    /**
     * Footer
     * @hooked blossom_shop_footer_start  - 10
     * @hooked blossom_shop_footer_two    - 30
     * @hooked blossom_shop_footer_bottom - 40
     * @hooked blossom_shop_footer_end    - 50
    */
    do_action( 'blossom_shop_footer' );
    
    /**
     * After Footer
     * 
     * @hooked blossom_shop_page_end    - 20
    */
    do_action( 'blossom_shop_after_footer' );
?>



<nav id="mobile" class="mobile-navigation">
    <a class="menu-toggle" id="openMobileMenu">
        <div class="toggle-line"></div>
        <div class="toggle-line"></div>
        <div class="toggle-line"></div>				
    </a>
</nav>
<div id="mobilefooter">
    <div class="footersearch">
        <?php ram_shop_footer_search(); ?>    
    </div>

    <div class="footercart">
        <?php     
            $ed_cart   = get_theme_mod( 'ed_shopping_cart', true ); 
            if( blossom_shop_is_woocommerce_activated() && $ed_cart ) blossom_shop_wc_cart_count(); 
        ?>
    </div>             
</div>

<div id="mobileMenu" class="modal">
		<span class="modalclose">&times;</span>
		<div class="modal-content">

			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'mobile_menu',
						'menu_id'        => 'mobile_menu',
					)
				)
				?>
		</div>
	</div>
<?php wp_footer(); ?>

</body>
</html>