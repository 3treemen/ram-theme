<?php
/**
 * Ram-theme Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ram-theme
 */

require_once('inc/metaboxes.php');


add_action( 'wp_enqueue_scripts', 'blossom_shop_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function blossom_shop_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'blossom-shop-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script( 'ram-theme-custom', get_stylesheet_directory_uri() . '/js/ram.js', array(), _S_VERSION, true );

	wp_enqueue_style( 'ram-theme-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'blossom-shop-style' )
	);

}

/**
 * add mobile menu
 */
if ( ! function_exists( 'ram_register_nav_menu' ) ) {
    function ram_register_nav_menu(){
        register_nav_menus( array(
            'mobile_menu' => __( 'Mobile Menu', 'ram-theme' ),
        ) );
    }
    add_action( 'after_setup_theme', 'ram_register_nav_menu', 0 );
}


if( ! function_exists( 'blossom_shop_header_search' ) ) :
/**
 * Header Search
*/
	function blossom_shop_header_search(){ 
		$ed_search = get_theme_mod( 'ed_header_search', true );
		if( $ed_search ) : ?>

			<div class="header-search">

				<button class="search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><path d="M86.065,85.194a6.808,6.808,0,1,0-.871.871L89.129,90,90,89.129Zm-1.288-.422a5.583,5.583,0,1,1,1.64-3.953A5.6,5.6,0,0,1,84.777,84.772Z" fill="#000000" transform="translate(-74 -74)"></path></svg> 
					<span class="search-title"><?php esc_html_e('Search', 'blossom-shop');?></span>
				</button><!-- .search-toggle -->
			
				<div class="header-search-wrap search-modal cover-modal" data-modal-target-string=".search-modal">

					<div class="header-search-inner-wrap">
						<?php get_search_form(); ?>
							<button class="close" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
								<span class="screen-reader-text"><?php _e( 'Close search', 'blossom-shop' ); ?></span>
							</button><!-- .search-toggle -->
					</div><!-- .search-modal-inner -->
				</div><!-- .menu-modal -->
			</div>

		<?php 
		endif; 
	}
endif;




if( ! function_exists( 'ram_shop_footer_search' ) ) :
	/**
	 * Footer Mobile Search
	*/
		function ram_shop_footer_search(){ 
			$ed_search = get_theme_mod( 'ed_header_search', true );
			if( $ed_search ) : ?>
	
				<div class="footer-search">
					<div class="footer-search-wrap">
						<div class="footer-search-inner-wrap">
							<?php get_search_form(); ?>		
						</div>
					</div>
				</div>
	
			<?php 
			endif; 
		}
	endif;

if( ! function_exists( 'blossom_shop_header' ) ) :
	/**
	 * Header Start
	*/
	function  blossom_shop_header(){ 
	
		$ed_cart   = get_theme_mod( 'ed_shopping_cart', true ); 
		?>
	
		<header id="masthead" class="site-header header-three" itemscope itemtype="http://schema.org/WPHeader">
			<?php if( has_nav_menu( 'secondary' ) || blossom_shop_social_links( false ) ) : ?>

				<div id="headerWrapper">
					<div id="headerLeft">
						<?php blossom_shop_site_branding(); ?>
					</div>
					<div id="headerRight">
						<div class="header-t">
							<?php blossom_shop_secondary_navigation(); ?>
							<?php if( blossom_shop_social_links( false ) ) : ?>
								<div class="right">
									<?php blossom_shop_social_links(); ?>
								</div>
							<?php endif; ?>
						</div><!-- .header-t -->
						<div class="header-main">
							<?php blossom_shop_primary_nagivation(); ?>
							<div class="right">
								<?php blossom_shop_header_search(); ?>
								<?php blossom_shop_user_block(); ?>
								<?php if( blossom_shop_is_woocommerce_activated() && $ed_cart ) blossom_shop_wc_cart_count(); ?>             
							</div>
						</div><!-- .header-main -->
					
					</div>
				</div>



			<?php endif; ?>
				</header><!-- #masthead -->
	<?php }
endif;
add_action( 'blossom_shop_header', 'blossom_shop_header', 20 );
		


if( ! function_exists( 'blossom_shop_primary_nagivation' ) ) :
	/**
	 * Primary Navigation.
	*/
	function blossom_shop_primary_nagivation(){ 
													  
		if ( function_exists( 'max_mega_menu_is_enabled' ) && max_mega_menu_is_enabled( 'primary' ) ) {
			wp_nav_menu( array( 'theme_location' => 'primary' ) );
		}else{
		?>
			<nav id="site-navigation" class="main-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
			   <!-- <button class="toggle-btn" data-toggle-target=".main-menu-modal" data-toggle-body-class="showing-main-menu-modal" aria-expanded="false" data-set-focus=".close-main-nav-toggle">
					<span class="toggle-bar"></span>
					<span class="toggle-bar"></span>
					<span class="toggle-bar"></span>
				</button> -->
				<div class="primary-menu-list main-menu-modal cover-modal" data-modal-target-string=".main-menu-modal">
					<button class="close close-main-nav-toggle" data-toggle-target=".main-menu-modal" data-toggle-body-class="showing-main-menu-modal" aria-expanded="false" data-set-focus=".main-menu-modal"></button>
					<div class="mobile-menu" aria-label="<?php esc_attr_e( 'Mobile', 'blossom-shop' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_id'        => 'primary-menu',
								'menu_class'     => 'nav-menu main-menu-modal',
								'fallback_cb'    => 'blossom_shop_primary_menu_fallback',
							) );
						?>
					</div>
				</div>
			</nav><!-- #site-navigation -->
		<?php
		}
	}
endif;


if( ! function_exists( 'blossom_shop_secondary_navigation' ) ) :
	/**
	 * Secondary Navigation
	*/
	function blossom_shop_secondary_navigation(){
		if( current_user_can( 'manage_options' ) || has_nav_menu( 'secondary' ) ) { ?>
		
			<nav class="secondary-menu">
				<!-- <button class="toggle-btn" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
					<span class="toggle-bar"></span>
					<span class="toggle-bar"></span>
					<span class="toggle-bar"></span>
				</button> -->
				<div class="secondary-menu-list menu-modal cover-modal" data-modal-target-string=".menu-modal">
					<button class="close close-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".menu-modal">
						<span class="toggle-bar"></span>
						<span class="toggle-bar"></span>
					</button>
					<div class="mobile-menu" aria-label="<?php esc_attr_e( 'Mobile', 'blossom-shop' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'secondary',
								'menu_id'        => 'secondary-menu',
								'menu_class'     => 'nav-menu menu-modal',
								'fallback_cb'    => 'blossom_shop_secondary_menu_fallback',
							) );
						?>
					</div>
				</div>
			</nav>
			<?php
		}
	}
	endif;


if( ! function_exists( 'blossom_shop_get_home_sections' ) ) :
	/**
	 * Returns Home Sections 
	*/
	function blossom_shop_get_home_sections(){
		$ed_banner     = get_theme_mod( 'ed_banner_section', 'slider_banner' );
		$home_sections = array( 'about', 'homeproduct', 'service', 'recent_product', 'featured', 'cat_one', 'testimonial', 'cta', 'blog', 'client' );
	
		$sections = array( 
			'about'       => array( 'sidebar' => 'about' ),
			'home_product' => array( 'sidebar' => 'home_product'),
			'service'     => array( 'sidebar' => 'service' ),
			'recent_product' => array( 'section' => 'recent_product' ),
			'featured'    => array( 'section' => 'featured' ),
			'cat_one'     => array( 'section' => 'cat_one' ),
			'testimonial' => array( 'sidebar' => 'testimonial' ),
			'cta'         => array( 'sidebar' => 'cta' ),
			'blog'        => array( 'section' => 'blog' ), 
			'client'      => array( 'sidebar' => 'client' ), 
		);
		
		$enabled_section = array();
		
		if( $ed_banner == 'static_banner' || $ed_banner == 'slider_banner' || $ed_banner == 'static_nl_banner' ) array_push( $enabled_section, 'banner' );
		
		foreach( $sections as $k => $v ){
			if( array_key_exists( 'sidebar', $v ) ){
				if( is_active_sidebar( $v['sidebar'] ) ) array_push( $enabled_section, $v['sidebar'] );
			}else{
				if( get_theme_mod( 'ed_' . $v['section'] . '_section', false ) ) array_push( $enabled_section, $v['section'] );
			}
		} 
		
		return apply_filters( 'blossom_shop_home_sections', $enabled_section );
	}
endif;


if( ! function_exists( 'blossom_shop_footer_bottom' ) ) :
	/**
	 * Footer Bottom
	*/
	function blossom_shop_footer_bottom(){ ?>
		<div class="footer-b">
			<div class="container">
				<div class="site-info">            
				<?php
					blossom_shop_get_footer_copyright();
					echo esc_html__( ' Reiki Aikido Meditation | Developed By ', 'ram-theme' ); 
					echo '<a href="' . esc_url( 'https://zoo.nl/' ) .'" rel="nofollow" target="_blank">' . esc_html__( 'Studio ZOO', 'ram-theme' ) . '</a>.';                
				
				?>               
				</div>
				<?php 
					blossom_shop_payment_method();
					blossom_shop_back_to_top(); 
				?>
			</div>
		</div>
		<?php
	}
endif;
add_action( 'blossom_shop_footer', 'blossom_shop_footer_bottom', 40 );

if( ! function_exists( 'blossom_shop_get_footer_copyright' ) ) :
	/**
	 * Footer Copyright
	*/
	function blossom_shop_get_footer_copyright(){
		$copyright = get_theme_mod( 'footer_copyright' );
		echo '<span class="copyright">';
		if( $copyright ){
			echo wp_kses_post( $copyright );
		}else{
			esc_html_e( '&copy; Copyright ', 'ram-theme' );
			echo date_i18n( esc_html__( ' Y', 'ram-theme' ) );
			echo ' <a href="' . esc_url( home_url( '/' ) ) . '">' . esc_html( get_bloginfo( 'name' ) ) . '</a>. ';
			esc_html_e( 'All Rights Reserved. ', 'blossom-shop' );
		}
		echo '</span>'; 
	}
endif;


if( ! function_exists( 'blossom_shop_content_start' ) ) :
/**
 * Content Start
 *   
*/
function blossom_shop_content_start(){       
    $home_sections      = blossom_shop_get_home_sections();
    $background_image   = blossom_shop_singular_post_thumbnail();
    $shop_background_image   = get_theme_mod( 'shop_background_image', false );
    $add_style_one = '';

    $shop_background_class = ( blossom_shop_is_woocommerce_activated() && is_shop() && $shop_background_image ) ? ' has-bgimg' : '';

    if( blossom_shop_is_woocommerce_activated() && is_shop() && $shop_background_image ) { 
        $add_style_one = 'style="background-image: url(\'' . esc_url( $shop_background_image ) . '\')"' ;
    }

    if ( ! is_front_page() && ! is_home() ) blossom_shop_breadcrumb();

    if( ! ( is_front_page() && ! is_home() && $home_sections ) ){ ?>
        <div id="content" class="site-content">            
        <?php if( ! is_home() && !( blossom_shop_is_woocommerce_activated() && is_product() ) ) : ?>
            <header class="page-header<?php echo esc_attr( $shop_background_class ); ?>" <?php if( is_singular() || is_404() ) : ?> style="background-image: url('<?php echo esc_url( $background_image ); ?>')" <?php endif; ?> <?php echo $add_style_one; ?>>
                <div class="container">
        			<?php        
                        if( is_archive() ){ 
                            if( is_author() ){
                                $author_title = get_the_author_meta( 'display_name' );
                                $author_description = get_the_author_meta( 'description' ); ?>
                                <div class="author-section">
                                    <figure class="author-img"><?php echo get_avatar( get_the_author_meta( 'ID' ), 120 ); ?></figure>
                                    <div class="author-content-wrap">
                                        <?php 
                                            echo '<h3 class="author-name">' . esc_html__( 'All Posts By: ','blossom-shop' ) . esc_html( $author_title ) . '</h3>';
                                            echo '<div class="author-content">' . wpautop( wp_kses_post( $author_description ) ) . '</div>';
                                        ?>      
                                    </div>
                                </div>
                                <?php 
                            }
                            else{
        					    the_archive_description( '<span class="sub-title">', '</span>' );
                                the_archive_title();
                            }             
                        }
                        
                        if( is_search() ){ 
                            echo '<span class="sub-title">' . esc_html__( 'SEARCH RESULTS FOR:', 'blossom-shop' ) . '</span>';
                            get_search_form();
                        }
                        
                        if( is_page() ){
                            // the_title( '<h1 class="page-title">', '</h1>' );
                        }

                        if( is_404() ) {
                            echo '<h1 class="page-title">' . esc_html__( 'Uh-Oh...','blossom-shop' ) . '</h1>';
                            echo '<div class="page-desc">' . esc_html__( 'The page you are looking for may have been moved, deleted, or possibly never existed.','blossom-shop' ) . '</div>';
                        }

                        if( is_single() ) {
                            blossom_shop_category();
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            if( 'post' === get_post_type() ){
                                echo '<div class="entry-meta">';
                                blossom_shop_posted_on();
                                blossom_shop_comment_count();
                                echo '</div>';
                            }
                        }
                    ?>
                </div>
    		</header>
        <?php endif;  ?>
            <div class="container">
    <?php 
    }
}
endif;
add_action( 'blossom_shop_content', 'blossom_shop_content_start' );


function blog_list($atts = [], $content = null) {
	// normalize
	$atts = array_change_key_case( (array) $atts, CASE_LOWER );

	$blog_atts = shortcode_atts( array(
		'blogcategory' => '', 
	), $atts );

	// extract( $atts );

	$args = array(
		'post_type' => 'post',
		'posts_per_page'   => -1,
		'category_name' => $blog_atts['blogcategory'],
		'order' => 'ASC',
	);

	$content = '';

	$hqr = new WP_Query( $args );

	if ($hqr->have_posts()) {
		$curid = get_the_ID();
		$content .= '<div class="bloglist">';
		$content .= '<h3>gerelateerde artikelen</h3>';
		$content .= '<ul>';
		while($hqr->have_posts()) {

			$hqr->the_post();
			$postid = $hqr->post->ID;
			if($curid != $postid) {
				$content .= '<li>';
				$content .= '<a href="'. get_post_permalink() .'">';
				$content .= get_the_title();
				$content .= '</a>';
				$content .= '</li>';	
			}
		}
		$content .= '</ul>';
		$content .= '</div>';
		wp_reset_postdata();
	}
	return $content;
}
function blog_shortcodes_init() {
	add_shortcode( 'bloglist', 'blog_list' );
}
add_action( 'init', 'blog_shortcodes_init' );

/**
 * Auto Complete all WooCommerce orders.
 */
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

// debug: show the used template
function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
			global $template;
			print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );

