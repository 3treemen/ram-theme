<?php
/**
 * Front Page
 * 
 * @package Blossom_Shop
 */

$home_sections = blossom_shop_get_home_sections();

if ( 'posts' == get_option( 'show_on_front' ) ) { //Show Static Blog Page
    include( get_home_template() );
}elseif( $home_sections ){ 
    get_header();
    //If any one section are enabled then show custom home page.
    echo '<div id="content" class="site-content">';
    foreach( $home_sections as $section ){
        get_template_part( 'sections/' . esc_attr( $section ) );  
    }
    ?>
    <section id="page_content">
    <div class="container">
    <?php
    the_content();
    ?>
    </div></section>

    <section id="lid">
        <div class="container">
            <h3>lid van</h3>
            <a href="https://shibumireiki.org/">
                <figure class="inline-block">
                    <img loading="lazy" width="250" height="49" 
                            src="/wp-content/themes/ram-theme/images/logo-1.png" alt="Shibumi Reiki" />
                </figure>
            </a>
            <a href="https://www.aikido-international.org/">
                <figure class="inline-block">
                    <img loading="lazy" width="100" height="100"
                            src="/wp-content/themes/ram-theme/images/iaf.jpg" alt="Internation Aikido Federation" />
                </figure>
            </a>
        </div>
    </section>
    <?php
    echo '</div>';
    get_footer();
}else {
    //If all section are disabled then show respective page template. 
    include( get_page_template() );
}