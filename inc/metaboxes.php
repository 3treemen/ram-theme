<?php


// Product Meta Fields
// create the field wrapper
function add_custom_meta_box() {
    $post_types = array ( 'product', 'post' );
    foreach( $post_types as $post_type) 
    {
        add_meta_box(
            'custom_meta_box', // $id
            'custom', // $title
            'custom_meta_box_markup', // $callback
            $post_type, // $screen
            'side', // $context
            'high', // $priority
            null
        );
    }
}
add_action( 'add_meta_boxes', 'add_custom_meta_box' );

// create the input fields
function custom_meta_box_markup($object) {
	$meta = get_post_meta( $object->ID );
	$show_on_homepage = ( isset( $meta['show-on-homepage'][0] ) &&  '1' === $meta['show-on-homepage'][0] ) ? 1 : 0;
	$terugblik = ( isset( $meta['terugblik'][0] ) &&  '1' === $meta['terugblik'][0] ) ? 1 : 0;

	wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
	<div>
			<label>
				Show on Homepage<br>
				<input type="checkbox" name="show-on-homepage" value="1" <?php checked( $show_on_homepage, 1 ); ?> />
			</label>	
		</div>

<?php  
}

// save the fields on update event
function save_custom_meta_box($post_id, $post, $update) {
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    // $post_types = array ( 'product', 'post' );
    // $slug = "product";
    // if($slug != $post->post_type)
	// 	return $post_id;
		
	$show_on_homepage = ( isset( $_POST['show-on-homepage'] ) && '1' === $_POST['show-on-homepage'] ) ? 1 : 0;
	update_post_meta( $post_id, 'show-on-homepage', esc_attr( $show_on_homepage ) );


}
add_action("save_post", "save_custom_meta_box", 10, 3);
// end Product Meta Fields

